package com.example.condorsoccer.listeners

import com.example.condorsoccer.dtos.DetailDto

interface TeamDetailListener {
    fun onStarted()
    fun onSuccess(response: DetailDto)
    fun onFailure(message: String)
}