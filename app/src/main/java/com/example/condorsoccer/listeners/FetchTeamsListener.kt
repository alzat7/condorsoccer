package com.example.condorsoccer.listeners

import com.example.condorsoccer.dtos.TeamDto

interface FetchTeamsListener {
    fun onStarted()
    fun onSuccess(response: List<TeamDto>)
    fun onFailure(message: String)
}