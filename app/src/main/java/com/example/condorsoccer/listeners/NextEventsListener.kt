package com.example.condorsoccer.listeners

import com.example.condorsoccer.dtos.EventDto

interface NextEventsListener {
    fun onStartedNextEvents()
    fun onSuccessNextEvents(response: List<EventDto>)
    fun onFailureNextEvents(message: String)
}