package com.example.condorsoccer.dtos

data class DetailDto(
    var teamName: String? = null,
    var teamBadge: String? = null,
    var teamJersey: String? = null,
    var formedYear: Int? = null,
    var descriptionEN: String? = null,
    var youtubeUrl: String? = null,
    var websiteUrl: String? = null,
    var facebookUrl: String? = null,
    var twitterUrl: String? = null,
    var instagramUrl: String? = null
)