package com.example.condorsoccer.dtos

data class EventDto(
    var eventName: String? = null,
    var league: String? = null,
    var date: String? = null
)