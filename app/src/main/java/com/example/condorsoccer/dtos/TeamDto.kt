package com.example.condorsoccer.dtos

data class TeamDto(
    var idTeam: Int? = null,
    var teamBadge: String? = null,
    var teamName: String? = null,
    var stadium: String? = null
)