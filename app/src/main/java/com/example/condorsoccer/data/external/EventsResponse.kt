package com.example.condorsoccer.data.external

data class EventsResponse<T>(
    val events: List<T>?
)