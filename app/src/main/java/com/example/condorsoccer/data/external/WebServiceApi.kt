package com.example.condorsoccer.data.external

import com.example.condorsoccer.models.MatchEvent
import com.example.condorsoccer.models.Team
import com.example.condorsoccer.models.TeamDetail
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface WebServiceApi {

    @GET("search_all_teams.php?")
    suspend fun fetchAllTeams(
        @Query("s") sport: String,
        @Query("c") country: String
    ) : Response<TeamResponse<Team>>

    @GET("lookupteam.php?")
    suspend fun teamDetail(
        @Query("id") idTeam: Int
    ) : Response<TeamResponse<TeamDetail>>

    @GET("eventsnext.php?")
    suspend fun nextEvents(
        @Query("id") idTeam: Int
    ) : Response<EventsResponse<MatchEvent>>

    companion object{
        operator fun invoke() : WebServiceApi{
            return Retrofit.Builder()
                .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebServiceApi::class.java)
        }
    }
}