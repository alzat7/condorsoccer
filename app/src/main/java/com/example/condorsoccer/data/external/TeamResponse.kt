package com.example.condorsoccer.data.external

data class TeamResponse<T>(
    val teams: List<T>?
)