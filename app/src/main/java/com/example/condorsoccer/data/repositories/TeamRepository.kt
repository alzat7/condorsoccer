package com.example.condorsoccer.data.repositories

import com.example.condorsoccer.data.external.EventsResponse
import com.example.condorsoccer.data.external.TeamResponse
import com.example.condorsoccer.data.external.WebServiceApi
import com.example.condorsoccer.models.MatchEvent
import com.example.condorsoccer.models.Team
import com.example.condorsoccer.models.TeamDetail
import retrofit2.Response

class TeamRepository {
    suspend fun fecthAllTeams(sport: String, country: String) : Response<TeamResponse<Team>>{

        return WebServiceApi().fetchAllTeams(sport, country)
    }

    suspend fun getDetails(idTeam: Int) : Response<TeamResponse<TeamDetail>>{
        return WebServiceApi().teamDetail(idTeam)
    }

    suspend fun getNextEvents(idTeam: Int) : Response<EventsResponse<MatchEvent>>{
        return WebServiceApi().nextEvents((idTeam))
    }
}