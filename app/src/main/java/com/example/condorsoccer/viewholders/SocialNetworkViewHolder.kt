package com.example.condorsoccer.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.social_networks_item.view.*

class SocialNetworkViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val socialNetwork = view.team_social_network
}