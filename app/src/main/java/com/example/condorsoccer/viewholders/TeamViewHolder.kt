package com.example.condorsoccer.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.team_item.view.*

class TeamViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    val teambadge = view.team_badge
    val teamName = view.team_name
    val teamStadium = view.team_stadium
}