package com.example.condorsoccer.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.event_item.view.*

class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val league = view.league_event
    val dateEvent = view.date_event
    val matchEvent = view.match_event
}