package com.example.condorsoccer.models

data class MatchEvent(
    var idEvent: Int? = null,
    var strEvent: String? = null,
    var strLeague: String? = null,
    var strDate: String? = null,
    var strTime: String? = null
)