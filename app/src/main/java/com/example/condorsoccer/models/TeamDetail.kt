package com.example.condorsoccer.models

data class TeamDetail(
    var strTeam: String? = null,
    var strTeamShort: String? = null,
    var strTeamBadge: String? = null,
    var strTeamJersey: String? = null,
    var strLeague: String? = null,
    var intFormedYear: Int? = null,
    var strDescriptionEN: String? = null,
    var strYoutube: String? = null,
    var strWebsite: String? = null, 
    var strFacebook: String? = null,
    var strTwitter: String? = null,
    var strInstagram: String? = null
)