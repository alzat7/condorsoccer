package com.example.condorsoccer.models

class Team(
    var idTeam: Int? = null,
    var strTeamBadge: String? = null,
    var strTeam: String? = null,
    var strTeamShort: String? = null,
    var strAlternate: String? = null,
    var strStadium: String? = null
) {
}