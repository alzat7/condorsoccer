package com.example.condorsoccer.activities

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.condorsoccer.R
import com.example.condorsoccer.adapters.TeamAdapter
import com.example.condorsoccer.dtos.TeamDto
import com.example.condorsoccer.listeners.FetchTeamsListener
import com.example.condorsoccer.listeners.NextEventsListener
import com.example.condorsoccer.models.MatchEvent
import com.example.condorsoccer.models.Team
import com.example.condorsoccer.viewmodels.FetchTeamsViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), FetchTeamsListener{

    override var viewResource: Int = R.layout.activity_main
    var adapter: TeamAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startViewModel()
        createAdapter()
    }

    override fun startViewModel(){
        val viewModel = ViewModelProviders.of(this).get(FetchTeamsViewModel::class.java)
        viewModel.fetchTeamsListener = this
        viewModel.loadInBackground()
    }

    override fun createAdapter(){
        adapter = TeamAdapter(this)
        adapter?.onItemClick = {
            val intent = Intent(this@MainActivity, TeamDetailActivity::class.java)
            intent.putExtra("idTeam", it.idTeam)
            startActivity(intent)
        }
    }

    override fun onStarted() {
        progress_circular.visibility = View.VISIBLE
    }

    override fun onSuccess(response: List<TeamDto>) {
        progress_circular.visibility = View.GONE
        adapter?.setTeams(response)
        recyclerview_team_list.layoutManager = LinearLayoutManager(this)
        recyclerview_team_list.adapter = adapter
    }

    override fun onFailure(message: String) {
        progress_circular.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
