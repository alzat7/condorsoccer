package com.example.condorsoccer.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    abstract var viewResource : Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(viewResource)
    }

    abstract fun startViewModel()

    abstract fun createAdapter()
}