package com.example.condorsoccer.activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.condorsoccer.R
import com.example.condorsoccer.adapters.EventAdapter
import com.example.condorsoccer.adapters.SocialNetworkAdapter
import com.example.condorsoccer.dtos.DetailDto
import com.example.condorsoccer.dtos.EventDto
import com.example.condorsoccer.listeners.NextEventsListener
import com.example.condorsoccer.listeners.TeamDetailListener
import com.example.condorsoccer.viewmodels.EventsViewModel
import com.example.condorsoccer.viewmodels.TeamDetailViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_team.*

class TeamDetailActivity : BaseActivity(), TeamDetailListener, NextEventsListener {

    override var viewResource: Int = R.layout.activity_detail_team

    var idTeam: Int? = null
    var eventAdapter: EventAdapter? = null
    var socialNetworkAdapter: SocialNetworkAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idTeam = intent.getIntExtra("idTeam", 0)

        startViewModel()
        createAdapter()
    }

    override fun startViewModel(){
        val viewModelDetail = ViewModelProviders.of(this).get(TeamDetailViewModel::class.java)
        val viewModelEvents = ViewModelProviders.of(this).get(EventsViewModel::class.java)

        viewModelDetail.teamDetailListener = this
        viewModelEvents.nextEventsListener = this

        viewModelDetail.setIdTeam(idTeam!!)
        viewModelEvents.setIdTeam(idTeam!!)

        viewModelDetail.loadInBackground()
        viewModelEvents.loadInBackground()
    }

    override fun createAdapter(){
        eventAdapter = EventAdapter(this)
        socialNetworkAdapter = SocialNetworkAdapter(this)

        socialNetworkAdapter?.onItemClick = {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://${it}")
            startActivity(intent)
        }
    }

    private fun setupView(detail: DetailDto){
        Picasso.get().load(detail.teamBadge).into(team_badge)
        Picasso.get().load(detail.teamJersey).into(team_jersey)
        team_name.text = detail.teamName
        team_fundation_year.text = detail.formedYear.toString()
        team_description.text = detail.descriptionEN

        socialNetworkAdapter?.setSocialNetworks(makeSocialNetworkList(detail))
        team_social_network_recyclerview.layoutManager = LinearLayoutManager(this)
        team_social_network_recyclerview.adapter = socialNetworkAdapter
    }

    private fun setupEvents(events: List<EventDto>){
        eventAdapter?.setEvents(events)
        next_events_recyclerview.layoutManager = LinearLayoutManager(this)
        next_events_recyclerview.adapter = eventAdapter
    }

    private fun makeSocialNetworkList(detail: DetailDto) : List<String>{
        var socialNetworkList = ArrayList<String>()
        detail.facebookUrl?.let { socialNetworkList.add(it) }
        detail.instagramUrl?.let { socialNetworkList.add(it) }
        detail.twitterUrl?.let { socialNetworkList.add(it) }
        detail.youtubeUrl?.let { socialNetworkList.add(it) }
        detail.websiteUrl?.let { socialNetworkList.add(it) }

        var socialNetworArray = arrayOfNulls<String>(socialNetworkList.size)
        socialNetworkList.toArray(socialNetworArray)
        return socialNetworArray.filterNotNull().toList()
    }

    override fun onStarted() {
        progress_circular.visibility = View.VISIBLE
    }

    override fun onSuccess(response: DetailDto) {
        progress_circular.visibility = View.GONE
        setupView(response)
    }

    override fun onFailure(message: String) {
        progress_circular.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onStartedNextEvents() {
        progress_circular.visibility = View.VISIBLE
    }

    override fun onSuccessNextEvents(response: List<EventDto>) {
        progress_circular.visibility = View.GONE
        setupEvents(response)
    }

    override fun onFailureNextEvents(message: String) {
        progress_circular.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
