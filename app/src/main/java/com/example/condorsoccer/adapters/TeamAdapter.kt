package com.example.condorsoccer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.condorsoccer.R
import com.example.condorsoccer.dtos.TeamDto
import com.example.condorsoccer.viewholders.TeamViewHolder
import com.squareup.picasso.Picasso

class TeamAdapter(private val context: Context) : RecyclerView.Adapter<TeamViewHolder>() {

    var onItemClick: ((TeamDto) -> Unit)? = null
    private var teams = emptyList<TeamDto>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.team_item, parent, false)
        return TeamViewHolder(view)
    }

    override fun getItemCount(): Int {
        return teams.size
    }

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        val team = teams[position]

        Picasso.get().load(team.teamBadge).into(holder?.teambadge)

        holder?.teamName?.text = team.teamName
        holder?.teamStadium?.text = team.stadium

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(team)
        }
    }

    fun setTeams(teams: List<TeamDto>){
        this.teams = teams
        notifyDataSetChanged()
    }
}