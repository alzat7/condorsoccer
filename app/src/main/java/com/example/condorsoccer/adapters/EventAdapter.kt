package com.example.condorsoccer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.condorsoccer.R
import com.example.condorsoccer.dtos.EventDto
import com.example.condorsoccer.viewholders.EventViewHolder

class EventAdapter(private val context: Context) : RecyclerView.Adapter<EventViewHolder>() {

    private var events = emptyList<EventDto>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.event_item, parent, false)
        return EventViewHolder(view)
    }

    override fun getItemCount(): Int {
        return events.size
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val event = events[position]

        holder?.league?.text = event.league
        holder?.dateEvent?.text = event.date
        holder?.matchEvent?.text = event.eventName
    }

    fun setEvents(events: List<EventDto>){
        this.events = events
        notifyDataSetChanged()
    }
}