package com.example.condorsoccer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.condorsoccer.R
import com.example.condorsoccer.viewholders.SocialNetworkViewHolder

class SocialNetworkAdapter(private val context: Context) : RecyclerView.Adapter<SocialNetworkViewHolder>() {

    var onItemClick: ((String) -> Unit)? = null
    private var socialNetworks = emptyList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SocialNetworkViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.social_networks_item, parent, false)
        return SocialNetworkViewHolder(view)
    }

    override fun getItemCount(): Int {
        return socialNetworks.size
    }

    override fun onBindViewHolder(holder: SocialNetworkViewHolder, position: Int) {
        val socialNetwork = socialNetworks[position]
        holder?.socialNetwork?.text = socialNetwork
        holder?.itemView.setOnClickListener {
            onItemClick?.invoke(socialNetwork)
        }
    }

    fun setSocialNetworks(socialNetworks: List<String>){
        this.socialNetworks = socialNetworks
        notifyDataSetChanged()
    }
}