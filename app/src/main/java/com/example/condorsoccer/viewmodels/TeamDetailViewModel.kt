package com.example.condorsoccer.viewmodels

import androidx.lifecycle.ViewModel
import com.example.condorsoccer.data.external.Coroutines
import com.example.condorsoccer.data.repositories.TeamRepository
import com.example.condorsoccer.dtos.DetailDto
import com.example.condorsoccer.listeners.TeamDetailListener
import com.example.condorsoccer.models.TeamDetail

class TeamDetailViewModel : ViewModel() {

    var teamDetailListener: TeamDetailListener? = null
    var idTeam: Int? = null

    fun setIdTeam(idTeam: Int){
        this.idTeam = idTeam
    }

    fun loadInBackground(){
        teamDetailListener?.onStarted()
        Coroutines.main {
            val response = TeamRepository().getDetails(idTeam!!)
            if (response.isSuccessful){
                teamDetailListener?.onSuccess(mapper(response.body()?.teams!!.first()))
            }else{
                teamDetailListener?.onFailure("Error code: ${response.code()}")
            }
        }
    }

    private fun mapper(entity: TeamDetail) : DetailDto{
        return DetailDto(
            teamName = entity.strTeam,
            teamBadge = entity.strTeamBadge,
            teamJersey = entity.strTeamJersey,
            descriptionEN = entity.strDescriptionEN,
            facebookUrl = entity.strFacebook,
            instagramUrl = entity.strInstagram,
            twitterUrl = entity.strTwitter,
            youtubeUrl = entity.strYoutube,
            websiteUrl = entity.strWebsite
        )
    }
}