package com.example.condorsoccer.viewmodels

import androidx.lifecycle.ViewModel
import com.example.condorsoccer.data.external.Coroutines
import com.example.condorsoccer.data.repositories.TeamRepository
import com.example.condorsoccer.dtos.TeamDto
import com.example.condorsoccer.listeners.FetchTeamsListener
import com.example.condorsoccer.models.Team

class FetchTeamsViewModel : ViewModel() {

    var fetchTeamsListener: FetchTeamsListener? = null

    fun loadInBackground(){
        fetchTeamsListener?.onStarted()
        Coroutines.main {
            val response = TeamRepository().fecthAllTeams("Soccer", "Spain")
            if ( response.isSuccessful){

                fetchTeamsListener?.onSuccess(mapper(response.body()?.teams!!))
            }else{
                fetchTeamsListener?.onFailure("Error code: ${response.code()}")
            }
        }
    }

    private fun mapper(teamsEntity: List<Team>) : List<TeamDto>{
        var newList = ArrayList<TeamDto>()
        for (entity in teamsEntity){
            newList.add(TeamDto(
                idTeam =  entity.idTeam,
                teamBadge =  entity.strTeamBadge,
                teamName =  entity.strTeam,
                stadium =  entity.strStadium))
        }

        return newList.toList()
    }
}