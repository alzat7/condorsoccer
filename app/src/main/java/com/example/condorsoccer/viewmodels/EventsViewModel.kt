package com.example.condorsoccer.viewmodels

import androidx.lifecycle.ViewModel
import com.example.condorsoccer.data.external.Coroutines
import com.example.condorsoccer.data.repositories.TeamRepository
import com.example.condorsoccer.dtos.EventDto
import com.example.condorsoccer.listeners.NextEventsListener
import com.example.condorsoccer.models.MatchEvent

class EventsViewModel : ViewModel(){

    var nextEventsListener : NextEventsListener? = null
    var idTeam : Int? = null

    fun setIdTeam(idTeam: Int){
        this.idTeam = idTeam
    }

    fun loadInBackground(){
        nextEventsListener?.onStartedNextEvents()
        Coroutines.main {
            val response = TeamRepository().getNextEvents(idTeam!!)
            if (response.isSuccessful){
                response.body()?.events?.let { nextEventsListener?.onSuccessNextEvents(mapper(
                    response.body()?.events!!)) }
            }else{
                nextEventsListener?.onFailureNextEvents("Error code: ${response.code()}")
            }
        }
    }

    private fun mapper(eventsEntity: List<MatchEvent>) : List<EventDto>{
        var newList = ArrayList<EventDto>()
        for (entity in eventsEntity){
            newList.add(EventDto(
                eventName = entity.strEvent,
                league = entity.strLeague,
                date = entity.strDate))
        }

        return newList.toList()
    }
}